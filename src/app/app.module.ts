import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { ItemListModule} from './ItemListPage/itemList.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    ItemListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
