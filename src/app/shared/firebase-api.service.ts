import {Injectable} from '@angular/core';
import {Subject} from 'rxjs/Subject';
import {Item} from '../models/Item';

declare var firebase: any;

@Injectable()
export class FirebaseApiService {
  config: any = {
    apiKey: 'AIzaSyAuNm5CIHCsUsV3ENTtMnxFNElIiRxbM8k',
    authDomain: 'angular-course-b363c.firebaseapp.com',
    databaseURL: 'https://angular-course-b363c.firebaseio.com',
    projectId: 'angular-course-b363c',
    storageBucket: 'angular-course-b363c.appspot.com',
    messagingSenderId: '362166160084'
  };
  database: any;
  DBdata = new Subject<Item>();

  constructor() {
  }

  initDB(): void {
    // Initialize Firebase
    firebase.initializeApp(this.config);

    firebase.auth().signInAnonymously()
      .catch(function (error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;

        console.log(`Auth error. errorCode - ${errorCode}, errorMessage - ${errorMessage}`);
      });

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.database = firebase.database();
        this.loadData();
      }
    });
  }

  loadData(): void {
    const tobuys = firebase.database().ref('tobuy/');
    tobuys.on('value', (tobuyList) => {
      tobuyList.forEach(tobuy => {
        this.DBdata.next(tobuy.val());
      });
    });
  }

  add(item): void {
    const newId = +(new Date());

    item.id = newId;
    firebase.database().ref('tobuy/' + newId).set(item);
  }

  update(id, item): void {
    firebase.database().ref('tobuy/' + id).update(item);
  }

  remove(id): void {
    firebase.database().ref('tobuy/' + id).remove();
  }
}
