import {Component, OnInit} from '@angular/core';
import {FirebaseApiService} from '../../shared/firebase-api.service';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
  private items: Array<Item> = [];

  constructor(private fireBaseApiService: FirebaseApiService) {
    this.fireBaseApiService.initDB();
  }

  ngOnInit() {
    this.fireBaseApiService.DBdata.subscribe(
      data => {
        this.items = this.items.filter(item => data.id !== item.id);
        this.items.push(data);
      },
      err => console.log(err)
    );
  }

  handleChange(item): void {
    item.checked = !item.checked;
    this.fireBaseApiService.update(item.id, item);

  }

  add(): void {
    const testItem = {
      title: 'newItem -' + (+(new Date())),
      amount: Math.round(Math.random() * 100),
      checked: false
    };

    this.fireBaseApiService.add(testItem);
  }

  update(item): void {
    item.title = 'updatedItem -' + (+(new Date()));

    this.fireBaseApiService.update(item.id, item);
  }

  remove(item, index): void {
    this.items.splice(index, 1);
    this.fireBaseApiService.remove(item.id);
  }
}
