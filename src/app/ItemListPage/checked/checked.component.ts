import {Component, Input} from '@angular/core';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-checked',
  templateUrl: './checked.component.html',
  styleUrls: ['./checked.component.scss']
})
export class CheckedComponent {
  @Input() items: Item;

  constructor() { }

}
