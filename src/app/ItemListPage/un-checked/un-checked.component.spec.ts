import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnCheckedComponent } from './un-checked.component';

describe('UnCheckedComponent', () => {
  let component: UnCheckedComponent;
  let fixture: ComponentFixture<UnCheckedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnCheckedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnCheckedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
