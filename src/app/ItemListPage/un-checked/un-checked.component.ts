import {Component, Input, OnInit} from '@angular/core';
import {Item} from '../../models/Item';

@Component({
  selector: 'app-un-checked',
  templateUrl: './un-checked.component.html',
  styleUrls: ['./un-checked.component.scss']
})
export class UnCheckedComponent implements OnInit {
  @Input() items: Item;

  constructor() { }

  ngOnInit() {
  }

}
