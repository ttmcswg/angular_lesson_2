import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { ItemListComponent } from './item-list/item-list.component';
import { CheckedComponent } from './checked/checked.component';
import { UnCheckedComponent } from './un-checked/un-checked.component';

import { FirebaseApiService } from '../shared/firebase-api.service';
import { ItemComponent } from './item/item.component';

@NgModule({
  declarations: [
    ItemListComponent,
    CheckedComponent,
    UnCheckedComponent,
    ItemComponent
  ],
  imports: [
    BrowserModule
  ],
  exports: [
    ItemListComponent,
    CheckedComponent,
    UnCheckedComponent
  ],
  providers: [FirebaseApiService]
})
export class ItemListModule { }
